<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\UnitRumah;
class ProductController extends Controller
{
    function getUnit()
        {
            $unit = DB::table('unit')->get();
        }

    function createUnit()
        {
            DB::beginTransaction(); 

            try
                {
                    //validasi client untuk input
                    $this->validate($request,[
                        'kavling' => 'required', 
                        'blok' => 'required', 
                        'no_rumah' => 'required', 
                        'harga_rumah' => 'required', 
                        'luas_tanah' => 'required', 
                        'luas_bangunan' => 'required'
                    ]);

                    //save ke database
                    $kavling = $request->input('kavling'); 
                    $blok = $request->input('blok');
                    $no_rumah = $request->input('no_rumah');
                    $harga_rumah = $request->input('harga_rumah');
                    $luas_tanah = $request->input('luas_tanah');
                    $luas_bangunan = $request->input('luas_banguna');


                    // save ke database menggunakan metode eloquen
                    $unit = new UnitRumah;
                    $unit->kavling = $kavling;
                    $unit->blok = $blok;
                    $unit->no_rumah = $no_rumah;
                    $unit->harga_rumah = $harga_rumah;
                    $unit->luas_tanah = $luas_tanah;
                    $unit->luas_bangunan = $luas_bangunan;
                    $unit->save();

                    $unit = UnitRumah::get();

                    DB::commit(); //jika insert data sukses maka data akan dicommit dan disave ke database
                    return response()->json($unit, 200); //saat add user maka akan melakukan add $usrList
                }
            catch (\Exception $e)
                {
                    DB::rollback(); //dan jika gagal maka data akan dirollback
                    return response()->json(["message" => $e->getMessage()], 500); //500->internal server error
                }
        }

        public function deleteUnit(Request $request)
            {
       
                // DB::table('userMIg')->where('id','=',$id)->delete();
                 DB::delete('delete from unit where id = ?', [$request->id]);
                return "data berhasil dihapus";
            }


}
